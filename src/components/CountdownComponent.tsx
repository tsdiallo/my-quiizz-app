import { useEffect, useState } from "react";

function CountdownComponent({
  nb,
  onDone,
  isFinished,
}: {
  nb: number;
  onDone: Function;
  isFinished: boolean;
}) {
  const [seconds, setseconds] = useState(30);
  const [myInterval, setmyInterval] = useState();

  // Remettre les secondes à 30 quand nb change
  useEffect(() => {
    setseconds(30);
  }, [nb]);

  // Initialiser le timer
  useEffect(() => {
    let interval: any = null;
    interval = setInterval(() => {
      setseconds((prevState) => {
        if (prevState == 0) {
          return 30;
        }
        return prevState - 1;
      });
    }, 1000);

    setmyInterval(interval);
  }, []);

  // Executer la fonction onDone quand les secondes arrivent à 0
  useEffect(() => {
    if (seconds == 0) {
      onDone();
    }
  }, [seconds]);

  // Arreter le quizz à la dernière question
  useEffect(() => {
    if (isFinished) {
      clearInterval(myInterval);
    }
  }, [myInterval, nb]);

  return (
    <div>
      <h1> {seconds} s </h1>
    </div>
  );
}

export default CountdownComponent;
