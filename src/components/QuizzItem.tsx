import { useState } from "react";

function QuizzItem({nb , setnb  , questions} : {nb : number ,  setnb : Function , questions : any}) {

  const [selectedValue, setSelectedValue] = useState<number>(0);
  const [isSubmit, setisSubmit] = useState(false)

  let question: { choices: any; title: any; } | null = null

  if (nb < questions.length){
    question  = questions[nb]
  }

const showAnswerStatus = () => {
    let success = question?.choices[selectedValue - 1].isCorrect === true;
    if(success){
        return <div className="alert alert-success" role="alert">
        Vous avez trouvé
        </div>
    }
   return <div>
     <div className="alert alert-danger" role="alert">
                Mauvaise réponse
    </div>

    <div className="alert alert-warning" role="alert">
    La bonne réponse est : <br/>
             <b>
             { question?.choices.find((choice: { isCorrect: boolean; }) => choice.isCorrect === true)?.title}
             </b>
    </div>
   </div>

}


    const handlePrevious = () => {
      setnb(nb - 1)
  }

  const handleNext = () => {
      if(!isSubmit){
        setisSubmit(true);
        return
      }
      setisSubmit(false)
      setnb(nb+1)
  }

    return (
        <div className="container mt-5">
        <div className="d-flex justify-content-center row">
        { question ? 
          <div className="col-md-10 col-lg-10">
            <div className="border">
              <div className="question bg-white p-3 border-bottom">
                <div className="d-flex flex-row justify-content-between align-items-center mcq">
                  <h4>Projet Quiz</h4><span>({nb + 1} of {questions.length})</span>
                </div>
              </div>
             <div className="question bg-white p-3 border-bottom">

              {isSubmit && showAnswerStatus()}

              <div className="d-flex flex-row align-items-center question-title">
                  <h3 className="text-danger">Q.</h3>
                  <h5 className="mt-1 ml-2">{question.title}</h5>
               </div>

                {question.choices.map((choice: { title: String } , index: number) => <div key={index} className="ans ml-2">
                    <label className="radio"> 
                      <input disabled={isSubmit} onClick={(e : React.FormEvent<HTMLInputElement> ) =>  {
                        if(!isSubmit) setSelectedValue(parseInt(e.currentTarget.value)+1)
                      } } type="radio" name={"question"+nb} value={index} /> 
                        <span>{choice.title}</span>
                    </label>
                  </div> )}
               
                
              </div>
              <div className="d-flex flex-row justify-content-between align-items-center p-3 bg-white">


                  <button disabled={nb == 0} onClick={handlePrevious} className="btn btn-primary d-flex align-items-center btn-danger" type="button">
                      <i className="fa fa-angle-left mt-1 mr-1" />
                      &nbsp;previous
                  </button>


                  { <button disabled={selectedValue === 0}  onClick={handleNext} className="btn btn-primary border-success align-items-center btn-success" type="button">
                          {questions.length - 1 === nb ? "Finish" : "next"}
                    <i className="fa fa-angle-right ml-2" />
                  </button>}
                </div>
            </div>
          </div> : <div>


            <h1>Quizz terminé !</h1>
            
            
             </div>}
        </div>
      </div>
    )
}

export default QuizzItem
