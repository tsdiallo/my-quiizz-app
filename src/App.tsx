import { useState } from "react";
import "./App.css";
import CountdownComponent from "./components/CountdownComponent";
import QuizzItem from "./components/QuizzItem";
import questions from "./data/questions";

function App() {
  const [nb, setnb] = useState(0);
  const [isFinished, setisFinished] = useState(false);
  const onDone = () => {
    if (nb === questions.length) {
      setisFinished(true);
      return;
    }
    setnb((prevState) => {
      return prevState + 1;
    });
  };

  return (
    <div className="App text-center">
      <CountdownComponent nb={nb} onDone={onDone} isFinished={nb === 3} />
      <QuizzItem questions={questions} nb={nb} setnb={setnb} />
    </div>
  );
}

export default App;
