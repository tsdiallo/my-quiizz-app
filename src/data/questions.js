export default [
    {
        "title" : "Qu'est-ce qu'un QCM ?",
        choices : [
            {
                "title" : "Une question avec plusieurs propositions de réponse mais une seule bonne réponse possible",
                isCorrect : true
            },
            {
                "title" : "Une formulaire de la sécurité sociale",
            },
            {
                "title" : "Une question avec plusieurs propositions de réponse mais une plusieurs bonnes réponses possibles"
            },
          
        ]
    },


    {
        "title" : "question 2",
        choices : [
            {
                "title" : "titre choix 1",
            },
            {
                "title" : "titre choix 2",
                isCorrect : true

            },
            {
                "title" : "titre choix 3"
            },
          
        ]
    },
    {
        "title" : "question 3",
        choices : [
            {
                "title" : "titre choix 1",
            },
            {
                "title" : "titre choix 2",
                isCorrect : true

            },
            {
                "title" : "titre choix 3"
            },
          
        ]
    }
]